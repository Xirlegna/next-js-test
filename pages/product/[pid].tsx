import type { NextPage, GetStaticPaths, GetStaticProps } from 'next';

interface Product {
  id: string;
  title: string;
  description: string;
}

interface ProductDetailPageProps {
  product: Product;
}

const ProductDetailPage: NextPage<ProductDetailPageProps> = (props) => {
  const { product } = props;

  if (!props.product) {
    return <p>Loading...</p>;
  }

  return (
    <>
      <h1>{product.title}</h1>
      <p>{product.description}</p>
    </>
  );
};

const getData = async () => {
  const response = await fetch('http://localhost:3000/api/products');
  const data = await response.json();

  return data;
};

export const getStaticPaths: GetStaticPaths = async () => {
  const data = await getData();

  const pathsWithParams = data.products.map((product: Product) => ({
    params: { pid: product.id },
  }));

  return {
    paths: pathsWithParams,
    fallback: true,
  };

  /**
   * Approach
  return {
    paths: [{ params: { pid: 'p1' } }],
    fallback: 'blocking',
  };
  */
};

export const getStaticProps: GetStaticProps = async (context) => {
  const { params } = context;

  if (!params?.pid) {
    return {
      notFound: true,
    };
  }

  const productId = params.pid;

  const data = await getData();

  const product = data.products.find(
    (product: Product) => product.id === productId
  );

  if (!product) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      product,
    },
  };
};

export default ProductDetailPage;
