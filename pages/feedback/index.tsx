import { useRef, useState, FormEvent } from 'react';
import type { NextPage } from 'next';

interface Feedback {
  id: string;
  email: string;
  feedback: string;
}

const FeedbackPage: NextPage = () => {
  const [feedbacks, setFeedbacks] = useState<[]>([]);

  const emailInputRef = useRef<HTMLInputElement>(null);
  const feedbackInputRef = useRef<HTMLTextAreaElement>(null);

  const submitFormHandler = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const body = {
      email: emailInputRef.current?.value,
      feedback: feedbackInputRef.current?.value,
    };

    fetch('/api/feedback', {
      method: 'POST',
      body: JSON.stringify(body),
      headers: { 'Content-Type': 'application/json' },
    })
      .then((res) => res.json())
      .then((data) => console.log(data));
  };

  const loadFeedbackHandler = () => {
    fetch('/api/feedback')
      .then((res) => res.json())
      .then((data) => {
        setFeedbacks(data.feedbacks);
      });
  };

  return (
    <div>
      <h4>FeedbackPage</h4>
      <form onSubmit={submitFormHandler}>
        <div>
          <label htmlFor="email">Your Email Address</label>
          <input id="email" type="email" ref={emailInputRef} />
        </div>
        <div>
          <label htmlFor="feedback">Your Feedback</label>
          <textarea id="feedback" rows={5} ref={feedbackInputRef} />
        </div>
        <button>Send Feedback</button>
      </form>
      <hr />
      <button onClick={loadFeedbackHandler}>Load Feedback</button>
      <ul>
        {feedbacks.map((feedback: Feedback) => (
          <li key={feedback.id}>{feedback.feedback}</li>
        ))}
      </ul>
    </div>
  );
};

export default FeedbackPage;
