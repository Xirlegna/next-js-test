// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';

interface Product {
  id: string;
  title: string;
  description: string;
}

interface Products {
  products: Product[];
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Products>
) {
  res.status(200).json({
    products: [
      { id: 'p1', title: 'Product 1', description: 'This is product 1' },
      { id: 'p2', title: 'Product 2', description: 'This is product 2' },
      { id: 'p3', title: 'Product 3', description: 'This is product 3' },
    ],
  });
}
