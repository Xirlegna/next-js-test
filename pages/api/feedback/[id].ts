import type { NextApiRequest, NextApiResponse } from 'next';
import fs from 'fs';
import path from 'path';

interface Feedback {
  id: string;
  email: string;
  feedback: string;
}

const getFeedbacks = () => {
  const filePath = path.join(process.cwd(), 'data', 'feedback.json');

  const fileData = fs.readFileSync(filePath);
  const data = JSON.parse(fileData.toString());

  return data;
};

const handler = (req: NextApiRequest, res: NextApiResponse) => {
  const id = req.query.id;

  const feedbacks = getFeedbacks();
  const selectedFeedback = feedbacks.find(
    (feedback: Feedback) => feedback.id === id
  );

  res.status(200).json({ feedback: selectedFeedback });
};

export default handler;
