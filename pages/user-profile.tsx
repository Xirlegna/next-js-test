import type { NextPage, GetServerSideProps } from 'next';

interface UserProfileProps {
  username: string;
}

const UserProfile: NextPage<UserProfileProps> = (props) => {
  return <div>{props.username}</div>;
};

export default UserProfile;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { params, req, res } = context;

  return {
    props: {
      username: 'Norbert',
    },
  };
};
