import type { NextPage, GetStaticProps } from 'next';
import Link from 'next/link';

interface Product {
  id: string;
  title: string;
  description: string;
}

interface NextPageProps {
  products: Product[];
}

const Home: NextPage<NextPageProps> = (props) => {
  const { products } = props;

  return (
    <ul>
      {products.map((product: Product) => (
        <li key={product.id}>
          <Link href={`/product/${product.id}`}>{product.title}</Link>
        </li>
      ))}
    </ul>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const response = await fetch('http://localhost:3000/api/products');
  const data = await response.json();

  return {
    props: data,
  };
};

export default Home;
